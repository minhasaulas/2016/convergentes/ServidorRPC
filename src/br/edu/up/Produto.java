package br.edu.up;

import java.io.Serializable;

public class Produto implements Serializable {
	
	private static final long serialVersionUID = -5863927712740086955L;
	private Integer codigo;
	private String nome;
	private String descricao;
	
	@Override
	public String toString() {
		return codigo +  ";" + nome + ";" + descricao;
	}
	
	public Produto() {
	}

	public Produto(Integer codigo, String nome, String descricao) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}