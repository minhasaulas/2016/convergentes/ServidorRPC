package br.edu.up;

import java.io.IOException;
import java.util.UUID;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

public class ClienteRPC {

	public static void main(String[] args) {

		Connection conexao = null;
		Channel canal = null;
		String resposta = null;

		try {
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("localhost");
			conexao = factory.newConnection();
			canal = conexao.createChannel();
			
			String queueReposta = canal.queueDeclare().getQueue();
			QueueingConsumer consumidor = new QueueingConsumer(canal);
			canal.basicConsume(queueReposta, true, consumidor);
			
			String correlationId = UUID.randomUUID().toString();
			BasicProperties props = new BasicProperties().builder()
					.correlationId(correlationId)
					.replyTo(queueReposta)
					.build();
			
			canal.basicPublish("", "rpc", props, "2".getBytes("UTF-8"));
			
			while(true){
				QueueingConsumer.Delivery entrega = consumidor.nextDelivery();
				if (entrega.getProperties().getCorrelationId().equals(correlationId)){
					resposta = new String(entrega.getBody(), "UTF-8");
					break;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conexao != null) {
				try {
					conexao.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.println(resposta); 
	}
}